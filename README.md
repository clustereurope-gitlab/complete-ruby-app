### GitLab + Kubernetes deployments
The purpose of this repository is to show complete working example of Kubernetes deployment.

This repository contains following files:
1. `Dockerfile` - file which describes how to build Docker image of the application,
2. `.gitlab-ci.yml` - file which describes GitLab Pipeline with stage: build, review apps, staging, production,
3. `server.rb` - file which holds an application entrypoint,
4. `scripts` - directory which contains a Kubernetes deployment script.

### Dockerfile
Application is shipped in thin Docker container based on `ruby:2.4.0-alpine`.

```Dockerfile
FROM ruby:2.4.0-alpine
ADD ./ /app/
WORKDIR /app
ENV PORT 5000
EXPOSE 5000

CMD ["sh", "-c", "while :; do ruby ./server.rb; done"]
```

### Check if application is properly "Dockerized"

```bash
# build docker image of the application
docker build -t my-application .

# run application in background
docker run -d -p 5000:5000 my-application

# check if application is accessible
curl http://localhost:5000/
```

### Deployment

This project implements deployment pipelines that runs on Kubernetes Service that is integrated in
https://docs.gitlab.com/ce/user/project/integrations/kubernetes.html.

It does the following steps:
1. Build docker image and push it to GitLab Container Registry: `app_image`,
2. If run on non-master create an review app of the application,
3. If run on master create an staging app of the application,
4. If run on master and when manual button is pressed create a production app of the application.

### LICENSE

MIT, Kamil Trzciński, 2017, GitLab
