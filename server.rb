require 'webrick'

server = WEBrick::HTTPServer.new :Port => 5000

server.mount_proc '/' do |request, response|
  response.body = 'Nice to meet you!'
end

server.start
